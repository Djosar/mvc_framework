<?php
// Home Controller

namespace App\Controllers;

use \Core\View;

class Home extends \Core\Controller {

	/**
	 * BEFORE filter
	 *
	 * @return void
	*/
	protected function before() {
		//echo "(before) ";
	}

	/**
	 * AFTER filter
	 *
	 * @return void
	*/
	protected function after() {
		// echo " (after)";
	}

	/**
	 * Show the Index page
	 *
	 * @return void
	*/
	public function indexAction() {
		//echo "Index action Home controller";
		/*View::render('Home/index.php', [
			'name' => 'Luke',
			'colors' => ['red', 'green', 'blue']
		]);*/

		View::renderTemplate('Home/index.html', [
			'name' => 'Luke',
			'colors' => ['red', 'green', 'blue']	
		]);
	}
}