# mvc_framework

Ein PHP-basiertes Model-View-Control-Framework erstellt nach dem **udemy-Kurs** "[Build PHP MVC Framework from scratch](https://www.udemy.com/php-mvc-from-scratch/learn/v4/overview)".

## Directory-Übersicht:

* **App/**  
  Der Application Ordner enthält die 3 Komponenten des MVC-Frameworks (Models, Views und Controller), sowie die **Config.php**, die die Einstellungen der App beinhaltet.
	* **Controllers/**  
	  Hier befinden sich die **Controller**-Klassen der Anwendung.
	* **Models/**  
	  Hier befinden sich die **Models**-Klassen der Anwendung.
	* **Views/**  
	  Hier befinden sich die **Views**-Klassen der Anwendung.
	* **Config.php**  
	  Hier werden die Einstellungen der Anwendung verwaltet.
* **Core/**  
  Alle grundlegenden Klassen der Anwendungen befinden sich hier.
	* **Controller.php**  
	  Abstrakte Klasse die als Grundkonstrukt aller **Controller**-Klassen funktioniert.
	* **Error.php**  
	  **Fehler**-Klasse, die alle **Errors** und **Exceptions** behandelt und in Log-Files schreibt.
	* **Model.php**  
	  Abstrakte Klasse die als Grundkonstrukt aller **Model**-Klassen funktioniert. Hier wird die Datenbank der Anwendung mithilfe der **Config.php** geladen.
	* **Router.php**  
	  Hier wird das **Routing** der App kontrolliert. URLs werden bearbeitet und aufgeteilt, sodass entsprechende Methoden bestimmter Klassen mit Parametern aufgerufen werden können.
	* **View.php**  
	  Klasse die eine Methode zum Darstellen (*Rendern*) von HTML-Templates beinhaltet.
* **logs/**  
  Diese Directory beinhaltet die detailierten Fehler- und Exceptions-Logs der Anwendung. Neben des Datums und der Uhrzeit wird die genaue Exception- oder Error-Nachricht gespeichert.
* **public/**  
  Diese Directory beinhaltet den Front-Controller der Anwendung, in welchem der Composer-Autoloader und der Error-/Exception-handler eingebunden werden. Zusätzlich wird hier das **Routing** initialisiert.
* **vendor/**  
  In dieser Directory wird Dritt-Anbieter Software gespeichert.



	

